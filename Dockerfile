FROM ubuntu:18.04


#
# Build args with defaults, 
#
ARG CONTAINER_UID=1000
ARG CONTAINER_GID=1000
ARG CONTAINER_USER=joe
ARG CONTAINER_HOME=/workdir
ARG CONTAINER_ROOT_PASSWORD=secret



#
# First things first: Add a container user and set container root password for good measure.
#
RUN echo "root:$CONTAINER_ROOT_PASSWORD" | chpasswd root
RUN addgroup --gid $CONTAINER_GID $CONTAINER_USER
RUN adduser --home $CONTAINER_HOME --shell /bin/bash --disabled-password --gecos '' --uid $CONTAINER_UID --gid $CONTAINER_GID $CONTAINER_USER



#
# Install packages.
#
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update --yes
RUN apt-get install --yes curl ca-certificates xz-utils build-essential nasm python uuid-dev libc6-dev

# Cleanup
RUN apt-get -qq autoremove --yes \
    && apt-get -qq clean --yes \
    && rm -rf /var/lib/apt/lists/*
ENV DEBIAN_FRONTEND=



#
# Build syslinux.
#
USER $CONTAINER_USER
WORKDIR $CONTAINER_HOME
RUN mkdir syslinux

# Download and extract into directory name "syslinux" without version.
RUN curl "https://mirrors.edge.kernel.org/pub/linux/utils/boot/syslinux/Testing/6.04/syslinux-6.04-pre1.tar.xz" | \
	tar Jxv -C syslinux --strip-components=1
WORKDIR $CONTAINER_HOME/syslinux

# Patching various known build issues, https://wiki.syslinux.org/wiki/index.php?title=Building
#RUN sed -i gpxe/src/Makefile -e '/^CFLAGS/s/:=/:= -fno-pic/'
#ENV LDFLAGS=--no-dynamic-linker

# Nuke pre-built binaries from orbit, just to make sure.
#RUN rm -frv bios efi32 efi64

# Make!
#RUN make
# Don't do this.  Compilation fails, and I think it's unnecessary.


# Aha! Only build installer. Then use pre-built binaries.
# https://wiki.syslinux.org/wiki/index.php?title=Common_Problems#Official_Binaries
RUN make clean
RUN make installer

